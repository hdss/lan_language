#include "lex.cpp"
#include <vector>
#include "headers/ASTree.h"
#include "sources/parser.cpp"
#include "sources/semantic.cpp"
#include "sources/Compiler_x86.cpp"
#include "headers/IOlib.hpp"
#include "sources/ElfCreator.cpp"

int main() {
    light_buffer* cur_buf = nullptr;
    std::vector<Token> tokens = lexical_parser("test.gg", cur_buf);
    ASTBuilder builder(tokens);
    ASTree tree = builder.buildASTree();
    tree.print("tree.png");
    tree.saveTree("tree.save");
    SemanticChecker sem(tree);
    sem.doCheck();
    tree.print("tree.png");
    Compiler comp(sem.passTable(), &tree);
    light_buffer bin = comp.compile();
    FILE *fout = fopen("test.bin", "w");
    Init(bin, fout);
    fwrite(bin.buffer, bin.pointer, 1, fout);
    fclose(fout);
    return 0;
}