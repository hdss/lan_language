//
// Created by dm on 11/29/19.
//
#define ISZERO(node) (node->value == 0.0 && node->type == NUMBER)
#define ISONE(node) (node->value == 1.0 && node->type == NUMBER)

bool convolution(Node** node) {

    if ((*node)->is_calculable && (*node)->type != NUMBER) {
        VarTable vTable = getVarTable();
        FunctionTable fTable = getFunctionTable();
        OperatorTable opTable = getOperatorTable();
        FixedPoint val = (*node)->eval(vTable, opTable, fTable);
        Node* new_node = new Node(val);
        *node = new_node;
        return true;
    }
    if ((*node)->type == OPERATOR) {
        if ((*node)->id == MUL) {
            if (ISONE((*node)->left)) {
                *node = (*node)->right;
                return true;
            }
            if (ISONE((*node)->right)) {
                *node = (*node)->left;
                return true;
            }
            if (ISZERO((*node)->left)) {
                *node = (*node)->left;
                return true;
            }
            if (ISZERO((*node)->right)) {
                *node = (*node)->right;
                return true;
            }
        }
        if ((*node)->id == PLUS) {
            if (ISZERO((*node)->left)) {
                *node = (*node)->right;
                return true;
            }
            if (ISZERO((*node)->right)) {
                *node = (*node)->left;
                return true;
            }
        }
    }
    bool ans = false;
    if ((*node)->left)
        ans |= convolution(&(*node)->left);
    if ((*node)->right)
        ans |= convolution(&(*node)->right);
    return ans;
}