//
// Created by dm on 5/16/20.
//

#ifndef LAN_HEADERS_IOLIB_HPP_
#define LAN_HEADERS_IOLIB_HPP_

#include "OpCode.hpp"

/**
 * Addr -> RSI
 */
static std::vector<Instruction*> InChar = {
    new MOV(Args(RDI, 0)),
    new MOV(Args(RAX, 0)),
    new MOV(Args(RDX, 1)),
    new SYSCALL(),
};

/*
 * Addr ->  RSI
 */
static std::vector<Instruction*> OutChar = {
    new MOV(Args(RDI, 1)),
    new MOV(Args(RAX, 1)),
    new MOV(Args(RDX, 1)),
    new SYSCALL(),
};

static std::vector<Instruction*> Exit =  {
    new MOV(Args(RAX, 60)),
    new MOV(Args(RDI, 0)),
    new SYSCALL()
};

void produce(light_buffer& buffer, const std::vector<Instruction*>& code, std::vector<Instruction*>& listing) {
  for (auto&& inst : code) {
    inst->write(buffer);
  }
  listing.insert(listing.end(), code.begin(), code.end());
}

#endif //LAN_HEADERS_IOLIB_HPP_
