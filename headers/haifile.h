//
// Created by dm on 11/29/19.
//

#ifndef WOLFRAM_HAIFILE_H
#define WOLFRAM_HAIFILE_H
#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cassert>

struct light_buffer;

/*!
  \brief String without possession
  Light string that use external buffer to store chars.
 */
struct light_string {
    light_buffer *buf;
    int pos;
    int length;
    const char &operator[](int index) const;
    bool operator==(const light_string &str) const;
    bool operator==(const char *) const;
    char *getPtr() const;

};

/*!
 \brief String buffer with reallocate, append and pointer to element. light_string native integration.
*/
struct light_buffer {
    char *buffer;
    int buffer_size;
    int pointer;
    void write(const char *str, int len, int pos);
    void append(const char *str, int len);
    void append(light_string str);
    template <typename T>
    void append(const T* el);
    void append(char chr);
    bool is_end() const;
    void append(const char *str);
    char read(int pos);
    char *curBuffer();
    light_string copyToBuffer(light_string str);
    light_string copyToBuffer(const char *str);
    char getNext();
    explicit light_buffer(int initial_size)  ;
    light_buffer(char *buffer, int size, int pointer);
    light_buffer();
    ~light_buffer();
};

struct LightStringDoubleHashFunc {
    size_t operator()(const light_string &str, size_t probe = 0) const;
    explicit LightStringDoubleHashFunc(size_t M);

private:
    size_t M, a1, a2;
    size_t first_hash(const light_string &str) const;
    size_t second_hash(const light_string &str) const;
    size_t string_hash(const light_string &str, size_t a) const;
};

/*!
  Read all characters from file
  \param file Input file
  \param[out] size Size of text
  \return light_buffer of all characters
*/
light_buffer read_file(const char* filename);
/*!
  Divide buffer from file into lines
  \param buffer buffer with characters
  \param size buffer size
  \param[out] lines_number number of lines in file
  \return array of lines
*/
light_string *divide_text(char *buffer, int size, int *lines_number);

template<typename T>
void light_buffer::append(const T* el) {
  append(reinterpret_cast<const char*>(el), sizeof(T));
}

#endif //WOLFRAM_HAIFILE_H
