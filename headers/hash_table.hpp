//
// Created by dm on 10/20/19.
//

#ifndef UNTITLED_HASH_TABLE_HPP
#define UNTITLED_HASH_TABLE_HPP
#pragma once
#include <iostream>
#include <map>
#include <string>
#include <vector>

using std::pair;
using std::string;
using std::vector;

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent = 50, long long start_size = 128>
class HashTable {
private:
    struct ElemType {
        KeyType key = KeyType();
        ValueType value = ValueType();
        bool isDeleted = false;
        bool isNil = true;
    };
    vector<ElemType> table;
    HashFunc hash;
    long long size, capacity;
    inline bool check_load_factor() const;
    void re_hash();

public:
    void insert(const KeyType &key, const ValueType &value);
    void erase(const KeyType &key);
    pair<bool, ValueType> find(const KeyType &key) const;
    HashTable();
};

template <typename KeyType, typename HashFunc,
        long long max_load_factor_percent = 50, long long start_size = 8>
class HashSet : private HashTable<KeyType, bool, HashFunc,
        max_load_factor_percent, start_size> {
public:
    void insert(const KeyType &key);
    void erase(const KeyType &key);
    bool find(const KeyType &key) const;
    HashSet();
};

struct StringDoubleHashFunc {
    long long operator()(const string &str, long long probe = 0) const;
    StringDoubleHashFunc(long long M);

private:
    long long M, a1, a2;
    long long first_hash(const string &str) const;
    long long second_hash(const string &str) const;
    long long string_hash(const string &str, long long a) const;
};

struct IntDoubleHashFunc {
    long long operator()(const int number, long long probe = 0) const;
    IntDoubleHashFunc(long long M);

private:
    long long M;
    long long first_hash(const int number) const;
    long long second_hash(const int number) const;
};

IntDoubleHashFunc::IntDoubleHashFunc(long long M) : M(M) {}

long long IntDoubleHashFunc::operator()(const int number, long long probe) const {
    return (first_hash(number) + ((probe * second_hash(number)) % M)) % M;
}

long long IntDoubleHashFunc::first_hash(const int number) const {
    return number % M;
}

long long IntDoubleHashFunc::second_hash(const int number) const {
    return (number * 2 + 1) % M;
}

StringDoubleHashFunc::StringDoubleHashFunc(long long M) : M(M) {
    a1 = 257;
    a2 = 263;
}

long long StringDoubleHashFunc::operator()(const string &str, long long probe) const {
    return (first_hash(str) + ((probe * second_hash(str)) % M)) % M;
}

long long StringDoubleHashFunc::first_hash(const string &str) const {
    return string_hash(str, a1);
}

long long StringDoubleHashFunc::second_hash(const string &str) const {
    return (2*string_hash(str, a2) + 1) % M;
}

long long StringDoubleHashFunc::string_hash(const string &str, long long a) const {
    long long ans = 0;
    for (long long i = 0; i < str.size(); i++) {
        ans = ((ans * a) + (str[i] - 'a')) % M;
    }
    return ans;
}

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent, long long start_size>
HashTable<KeyType, ValueType, HashFunc, max_load_factor_percent,
        start_size>::HashTable()
        : size(0), capacity(start_size), table(start_size), hash(start_size) {}

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent, long long start_size>
inline bool HashTable<KeyType, ValueType, HashFunc, max_load_factor_percent,
        start_size>::check_load_factor() const {
    return ((1.0 *size / capacity) <= max_load_factor_percent / 100.0);
}

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent, long long start_size>
void HashTable<KeyType, ValueType, HashFunc, max_load_factor_percent,
        start_size>::re_hash() {
    capacity *= 2;
    vector<ElemType> new_table(capacity);
    hash = HashFunc(capacity);
    int new_n = 0;
    for (auto i : table) {
        if (!i.isNil && !i.isDeleted) {
            new_n++;
            for (long long probe = 0; probe < capacity; probe++) {
                long long cur_hash = hash(i.key, probe);
                if (new_table[cur_hash].isNil) {
                    new_table[cur_hash].key = i.key;
                    new_table[cur_hash].value = i.value;
                    new_table[cur_hash].isNil = false;
                    break;
                }
            }
        }
    }
    table.clear();
    size = new_n;
    table = std::move(new_table);
}

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent, long long start_size>
void HashTable<KeyType, ValueType, HashFunc, max_load_factor_percent,
        start_size>::insert(const KeyType &key, const ValueType &value) {
    if (!check_load_factor()) re_hash();
    for (long long probe = 0; probe < capacity; probe++) {
        long long cur_hash = hash(key, probe);
        if (table[cur_hash].isNil) {
            table[cur_hash].key = key;
            table[cur_hash].value = value;
            table[cur_hash].isNil = false;
            break;
        } else if (table[cur_hash].isDeleted) {
            table[cur_hash].key = key;
            table[cur_hash].value = value;
            table[cur_hash].isDeleted = false;
            size--;
            break;
        }
    }
    size++;
}

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent, long long start_size>
pair<bool, ValueType>
HashTable<KeyType, ValueType, HashFunc, max_load_factor_percent,
        start_size>::find(const KeyType &key) const {
    for (long long probe = 0; probe < capacity; probe++) {
        long long cur_hash = hash(key, probe);
        if (table[cur_hash].isNil)
            return {false, ValueType()};
        else if (!table[cur_hash].isDeleted && table[cur_hash].key == key)
            return {true, table[cur_hash].value};
    }
    return {false, ValueType()};
}

template <typename KeyType, typename ValueType, typename HashFunc,
        long long max_load_factor_percent, long long start_size>
void HashTable<KeyType, ValueType, HashFunc, max_load_factor_percent,
        start_size>::erase(const KeyType &key) {
    for (long long probe = 0; probe < capacity; probe++) {
        long long cur_hash = hash(key, probe);
        if (table[cur_hash].isNil)
            return;
        else if (table[cur_hash].key == key) {
            table[cur_hash].isDeleted = true;
            break;
        }
    }
}

template <typename KeyType, typename HashFunc, long long max_load_factor_percent,
        long long start_size>
HashSet<KeyType, HashFunc, max_load_factor_percent, start_size>::HashSet()
        : HashTable<KeyType, bool, HashFunc, max_load_factor_percent,
        start_size>::HashTable() {}

template <typename KeyType, typename HashFunc, long long max_load_factor_percent,
        long long start_size>
void HashSet<KeyType, HashFunc, max_load_factor_percent, start_size>::insert(
        const KeyType &key) {
    HashTable<KeyType, bool, HashFunc, max_load_factor_percent,
            start_size>::insert(key, true);
}

template <typename KeyType, typename HashFunc, long long max_load_factor_percent,
        long long start_size>
bool HashSet<KeyType, HashFunc, max_load_factor_percent, start_size>::find(
        const KeyType &key) const {
    return HashTable<KeyType, bool, HashFunc, max_load_factor_percent,
            start_size>::find(key)
            .first;
}

template <typename KeyType, typename HashFunc, long long max_load_factor_percent,
        long long start_size>
void HashSet<KeyType, HashFunc, max_load_factor_percent, start_size>::erase(
        const KeyType &key) {
    HashTable<KeyType, bool, HashFunc, max_load_factor_percent,
            start_size>::erase(key);
}

#endif //UNTITLED_HASH_TABLE_HPP