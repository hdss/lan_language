//
// Created by dm on 5/15/20.
//

#ifndef LAN_HEADERS_OPCODE_HPP_
#define LAN_HEADERS_OPCODE_HPP_
#include <string>
#include <utility>
#define REG_DEF(id, name)\
  static const Register name(id, #name);
struct Register {
  unsigned char id;
  bool is_REX;
  [[nodiscard]] unsigned char REG() const{
    return id;
  }
  std::string name;
  explicit Register(char id, std::string name) : id(id & 7), is_REX(id & 8), name(std::move(name)) {};
};


REG_DEF(0, RAX);
REG_DEF(1, RCX);
REG_DEF(2, RDX);
REG_DEF(3, RBX);
REG_DEF(4, RSP);
REG_DEF(6, RSI);
REG_DEF(5, RBP);
REG_DEF(7, RDI);
REG_DEF(8, R8);
REG_DEF(9, R9);
REG_DEF(10, R10);
REG_DEF(11, R11);
REG_DEF(12, R12);
REG_DEF(13, R13);
REG_DEF(14, R14);
REG_DEF(15, R15);

struct ModRM {
  unsigned char mod;
  Register r1, r2;

  ModRM(Register dest, Register source) : r1(std::move(dest)), r2(std::move(source)) {
    mod = 0b11000000;
  }
  ModRM(Register dest, Register source_addr, const char offset) :
  r2(std::move(dest)),
  r1(std::move(source_addr)) {
    mod = 0b01000000;
  }
  unsigned char get() const {
    return mod | r2.REG() << 3 | r1.REG();
  }
};

struct Instruction {
  virtual std::string AsText() const = 0;
  virtual void write(light_buffer& buf) const = 0;
};

enum class ArgsType {
  RegPair,
  RegMem,
  MemReg,
  RegImm,
};

struct Args  {
  ModRM mod;
  ArgsType type;
  char offset = 0;
  unsigned char rex = 0b01001000;
  long long imm = 0;
  Args(const Register& dest, const Register& source) : mod(dest, source), type(ArgsType::RegPair) {
    rex |= (source.is_REX << 2) | dest.is_REX;
  }
  Args(const Register& dest, const Register& source_addr, const char offset) : mod(dest, source_addr, offset), type(ArgsType::RegMem), offset(offset) {
    rex |= (dest.is_REX << 2) | source_addr.is_REX;
  }
  Args(const Register& dest_addr, const char offset, const Register& source) : mod(source, dest_addr, offset), type(ArgsType::MemReg), offset(offset) {
    rex |= (source.is_REX << 2) | dest_addr.is_REX;
  }
  Args(const Register& dest, long long imm64) : mod(dest, dest), type(ArgsType::RegImm), imm(imm64) {
    rex |= dest.is_REX;
  }
  std::string AsText() const{
    std::string ans;
    switch(type) {
      case ArgsType::RegPair: ans += mod.r1.name; ans += ", "; ans += mod.r2.name; break;
      case ArgsType::RegMem: ans += mod.r2.name; ans += ", ["; ans += mod.r1.name; ans += offset > 0 ? "+" : ""; ans += std::to_string(offset); ans += ']'; break;
      case ArgsType::MemReg: ans += "["; ans += mod.r1.name; ans += offset > 0 ? "+" : ""; ans += std::to_string(offset); ans += "], "; ans += mod.r2.name; break;
      case ArgsType::RegImm: ans += mod.r1.name; ans += ", "; ans += std::to_string(imm); break;
    }
    return ans;
  }
  void write(light_buffer& buffer) const{
    buffer.append(mod.get());
    if (type == ArgsType::RegImm) {
      buffer.append(&imm);
      return;
    }
    if (type != ArgsType::RegPair)
      buffer.append(offset);
  }
};

#define ARGS_INSTRUCTION(name, op1, op2, op3, op4)\
struct name : Instruction {\
  Args args_;\
  std::string AsText() const override {\
    return std::string(#name) + " " + args_.AsText();\
  }\
  explicit name(Args args) : args_(std::move(args)) {}\
  void write(light_buffer& buffer) const override {\
    buffer.append(args_.rex);\
    switch(args_.type) {\
      case ArgsType::RegPair: buffer.append(op1); break;\
      case ArgsType::RegMem: buffer.append(op2); break;\
      case ArgsType::MemReg: buffer.append(op3); break;\
      case ArgsType::RegImm: buffer.append(op4 + (op1 == 0x89) * args_.mod.r1.id); break;\
    }\
    args_.write(buffer);\
  }\
};

struct MOV : Instruction {
  Args args_;
  std::string AsText() const override {
    return "mov " + args_.AsText();
  }
  explicit MOV(Args args) : args_(std::move(args)) {}
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex);
    switch(args_.type) {
      case ArgsType::RegPair: buffer.append(0x89); break;
      case ArgsType::RegMem: buffer.append(0x8B); break;
      case ArgsType::MemReg: buffer.append(0x89); break;
      case ArgsType::RegImm: buffer.append(0xB8 + args_.mod.r1.id); break;
    }
    if (ArgsType::RegImm == args_.type) {
      buffer.append(&args_.imm);
    } else
      args_.write(buffer);
  }
};

struct ADD : Instruction {
  Args args_;
  std::string AsText() const override {
    return "add " + args_.AsText();
  }
  explicit ADD(Args args) : args_(std::move(args)) {
    if (args.type == ArgsType::RegImm)
      args.mod.r2 = RAX; // /0 add
  }
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex);
    switch(args_.type) {
      case ArgsType::RegPair: buffer.append(0x01); break;
      case ArgsType::RegMem: buffer.append(0x03); break;
      case ArgsType::MemReg: buffer.append(0x01); break;
      case ArgsType::RegImm: buffer.append(0x83); break;
    }
    args_.write(buffer);
  }
};

struct SUB : Instruction {
  Args args_;
  std::string AsText() const override {
    return "sub " + args_.AsText();
  }
  explicit SUB(Args args) : args_(std::move(args)) {
    if (args.type == ArgsType::RegImm)
      args.mod.r2 = RBP; // /5 add
  }
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex);
    switch(args_.type) {
      case ArgsType::RegPair: buffer.append(0x29); break;
      case ArgsType::RegMem: buffer.append(0x2B); break;
      case ArgsType::MemReg: buffer.append(0x29); break;
      case ArgsType::RegImm: buffer.append(0x83); break;
    }
    args_.write(buffer);
  }
};

struct CALL : Instruction {
  int offset;
  explicit CALL(int offset = 0) : offset(offset) {}
  std::string AsText() const override {
    return "call " + std::to_string(offset);
  }
  void write(light_buffer& buffer) const override {
    buffer.append(0xE8);
    buffer.append(&offset);
  }
};

struct IMUL : Instruction {
  Args args_;
  explicit IMUL(Args args) : args_(std::move(args)) {}
  std::string AsText() const override {
    return "imul " + args_.AsText();
  };
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex);
    switch (args_.type) {
      case ArgsType::RegPair: buffer.append(0x0F); buffer.append(0xAF); break;
      case ArgsType::RegMem: buffer.append(0x0F); buffer.append(0xAF); break;
      default: throw std::logic_error("mul not defined for that args: " + args_.AsText());
    }
    args_.write(buffer);
  }
};

struct IDIV : Instruction {
  Args args_;
  IDIV(const Register& dividor) : args_(dividor, dividor) {}
  IDIV(Register&dividor_addr, const char offset) : args_(dividor_addr, dividor_addr, offset) {}
  std::string AsText() const override {
    return "idiv " + args_.AsText();
  }
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex);
    switch (args_.type) {
      case ArgsType::RegPair: buffer.append(0xF7); break;
      case ArgsType::RegMem: buffer.append(0xF7); break;
      default: throw std::logic_error("idiv is not defined for : " + args_.AsText());
    }
    args_.write(buffer);
  }
};

struct JMP : Instruction {
  int offset;
  explicit JMP(int offset = 0) : offset(offset) {}
  std::string AsText() const override {
    return "jmp " + std::to_string(offset);
  }
  void write(light_buffer& buffer) const override {
    buffer.append(0xE9);
    buffer.append(&offset);
  }
};

struct JE : Instruction {
  int offset;
  explicit JE(int offset = 0) : offset(offset) {}
  std::string AsText() const override {
    return "je " + std::to_string(offset);
  }
  void write(light_buffer& buffer) const override {
    buffer.append(0x0F);
    buffer.append(0x84);
    buffer.append(&offset);
  }
};

struct JNE : Instruction {
  int offset;
  explicit JNE(int offset = 0) : offset(offset) {}
  std::string AsText() const override {
    return "jne " + std::to_string(offset);
  }
  void write(light_buffer& buffer) const override {
    buffer.append(0x0F);
    buffer.append(0x85);
    buffer.append(&offset);
  }
};


struct CMP : Instruction {
  Args args_;
  explicit CMP(Args args) : args_(std::move(args)) {}
  std::string AsText() const override {
    return "cmp " + args_.AsText();
  }
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex);
    switch (args_.type) {
      case ArgsType::RegPair: buffer.append(0x39); break;
      case ArgsType::RegMem: buffer.append(0x3B); break;
      case ArgsType::MemReg: buffer.append(0x39); break;
      case ArgsType::RegImm: if(args_.imm > INT32_MAX ) throw std::logic_error("INT OVERFLOW"); break;
    }
    args_.write(buffer);
  }
};

#define SET_INSTRUCTION(nm, opcode)\
struct nm: Instruction {\
  Register reg;\
  explicit nm(Register  reg) : reg(std::move(reg)) {}\
  std::string AsText() const override {\
    return std::string(#nm) + " " + reg.name;\
  }\
  void write(light_buffer& buffer) const override{\
    Args args(reg, reg);\
    buffer.append(0x0F);\
    buffer.append(opcode);\
    buffer.append(args.mod.get());\
  }\
};

SET_INSTRUCTION(SETG, 0x9F)
SET_INSTRUCTION(SETL, 0x9C)
SET_INSTRUCTION(SETLE, 0x9E);
SET_INSTRUCTION(SETGE, 0x9D);
SET_INSTRUCTION(SETE, 0x94);

struct RET : Instruction {
  std::string AsText() const override {
    return "ret";
  }
  RET() = default;
  void write(light_buffer& buffer) const override {
    buffer.append(0xC3);
  }
};

struct SYSCALL : Instruction {
  std::string AsText() const override {
    return "syscall";
  }
  SYSCALL()  = default;
  void write(light_buffer& buffer) const override {
    buffer.append(0x0F);
    buffer.append(0x05);
  }
};

struct PUSH : Instruction {
  Register reg;
  std::string AsText() const override {
    return "push " + reg.name;
  }
  PUSH(Register reg) : reg(std::move(reg)) {}
  void write(light_buffer &buffer) const override {
    buffer.append(0x50 + reg.REG());
  }
};

struct POP : Instruction {
  Register reg;
  std::string AsText() const override {
    return "pop " + reg.name;
  }
  POP(Register reg) : reg(std::move(reg)) {}
  void write(light_buffer &buffer) const override {
    buffer.append(0x58 + reg.REG());
  }
};

struct MOVZX : Instruction {
  Args args_;
  std::string AsText() const override {
    return "movzx " + args_.AsText();
  }
  template<typename ...T>
  MOVZX(T&&... arg) : args_(arg...) {}
  void write(light_buffer& buffer) const override {
    buffer.append(args_.rex & 0b11111110);
    buffer.append(0x0F);
    buffer.append(0xB6);
    args_.write(buffer);
  }
};

#endif //LAN_HEADERS_OPCODE_HPP_
