//
// Created by dm on 12/6/19.
//

#pragma once
#include <utility>
#include <vector>
#include "haifile.h"

enum class NodeType {
    ROOT,
    DATA,
    FUNC,
    ARG,
    STAT,
    COMPOUND,
    ADD,
    SUB,
    MUL,
    DIV,
    NUM,
    ASS,
    ADD_ASS,
    SUB_ASS,
    MUL_ASS,
    DIV_ASS,
    IF,
    WHILE,
    RETURN,
    LESS,
    GREATER,
    LEQ,
    GREQ,
    EQ,
    VAR,
    IN,
    OUT
};

class ASTree {
private:
    struct Node {
        std::vector<Node*> children;
        light_string value;
        int id;
        NodeType type;
        Node(NodeType type, int id) : id(id), type(type) {}
        Node* make_copy(const Node *node) const;
        size_t addr = 0;
        size_t end_addr = 0;
        char *getText();
    };

    bool convolute(Node **node);

    Node *make_copy (const Node &node) const;
    Node *root;
    light_buffer buf;
    int size;
    friend class ASTBuilder;
    friend class SemanticChecker;
    friend class SymbolsTable;
    friend class Compiler;
    void writeDfs(Node *node, light_buffer *buf, bool is_data);
    static int nextChar(light_buffer *buffer);
    static light_string readData(light_buffer *buffer);
public:
    ASTree();
    void saveTree(const char *filename);
    void printDfs(Node *node, FILE *file);

    void print(const char *filename);

    void conv();

    light_buffer serialize();
    friend ASTree deserialize(light_buffer *buf);
    friend Node* readDfs(light_buffer *buf, bool is_func);
};

