//
// Created by dm on 12/20/19.
//

#ifndef LAN_TOKENS_HPP
#define LAN_TOKENS_HPP
#include "haifile.h"
enum class TokenType {
    ID,
    NUMBER,
    OPERATOR,
    SEPARATOR,
    KEYWORD
};

struct Token {
    TokenType type;
    light_string value;
};

#endif //LAN_TOKENS_HPP
