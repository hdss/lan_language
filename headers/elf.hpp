//
// Created by dm on 5/19/20.
//


#ifndef LAN_HEADERS_ELF_HPP_
#define LAN_HEADERS_ELF_HPP_
#include <elf.h>
#pragma pack(push, 1)

struct ELF_Header {
  __uint32_t ei_MAG         = 0x464C457F;
  __uint8_t ei_CLASS       = ELFCLASS64;
  __uint8_t ei_DATA        = ELFDATA2LSB;
  __uint32_t ei_VERSION     = 0x00000001;
  __uint16_t ei_OSABI       = ELFOSABI_NONE;
  __uint32_t ei_OSABIVER    = 0x00000000;
  __uint16_t E_TYPE         = ET_EXEC;
  __uint16_t E_MACHINE      = EM_X86_64;
  __uint32_t E_VERSION      = EV_CURRENT;
  __uint64_t E_ENTRY        = 0x0000000000400080;
  __uint64_t E_PHOFF        = 0x0000000000000040;
  __uint64_t E_SHOFF        = 0x0000000000000000;
  __uint32_t E_FLAGS        = 0x00000000;
  __uint16_t E_EHSIZE       = 0x0040;
  __uint16_t E_PHENTSIZE    = 0x0038;
  __uint16_t E_PHNUM        = 0x0001;
  __uint16_t E_SHENTSIZE    = 0x0040;
  __uint16_t E_SHNUM        = 0x0005;
  __uint16_t E_SHSTRNDX     = 0x0004;
};

struct Program_Header {
  __uint32_t P_TYPE    = 0x00000000 | PT_LOAD;
  __uint32_t P_FLAGS   = 0x00000000 | PF_R | PF_X;
  __uint64_t P_OFFSET  = 0x0000000000000000;
  __uint64_t P_VADDR   = 0x0000000000400000;
  __uint64_t P_PADDR   = 0x0000000000400000;
  __uint64_t P_FILESZ  = 0x0000000000000080;
  __uint64_t P_MEMSZ   = 0x0000000000000080;
  __uint64_t P_ALIGN   = 0x0000000000200000;
  __uint64_t zeoroz    = 0x0000000000000000;
};
#pragma pack(pop)
#endif //LAN_HEADERS_ELF_HPP_
