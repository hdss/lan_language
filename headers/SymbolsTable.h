//
// Created by dm on 12/20/19.
//

#ifndef LAN_SYMBOLSTABLE_H
#define LAN_SYMBOLSTABLE_H
#include "haifile.h"
#include "hash_table.hpp"

struct SymbolsTable {
    HashTable<light_string, ASTree::Node*, LightStringDoubleHashFunc> funcs;
    HashTable<light_string, int, LightStringDoubleHashFunc> vars;
    int funcs_sz;
    int vars_sz;
};
#endif //LAN_SYMBOLSTABLE_H
