%{
#pragma once
#include <stdio.h>
#include <vector>
#include "headers/tokens.hpp"
using std::vector;
vector<Token> _tokens;
light_buffer _buf(128);
%}

digit         [0-9]
letter        [a-zA-Z]

%%

{digit}+                                   {Token new_token; new_token.type = TokenType::NUMBER; new_token.value = _buf.copyToBuffer(yytext); _tokens.push_back(new_token);}
[+*/<>]|"<="|"=="|">="|"!="|"&&"|"||"|"-"|"+="|"-="|"*="|"="   {Token new_token; new_token.type = TokenType::OPERATOR; new_token.value = _buf.copyToBuffer(yytext); _tokens.push_back(new_token);}
[ \t\r\n\f]    ; /* ignore whitespace */
[();,{}$]                                   {Token new_token; new_token.type = TokenType::SEPARATOR; new_token.value = _buf.copyToBuffer(yytext); _tokens.push_back(new_token);}
"if"|"while"|"return"|"in"|"out"|"let"     {Token new_token; new_token.type = TokenType::KEYWORD; new_token.value = _buf.copyToBuffer(yytext); _tokens.push_back(new_token);}
{letter}({letter}|{digit})*                {Token new_token; new_token.type = TokenType::ID; new_token.value = _buf.copyToBuffer(yytext); _tokens.push_back(new_token);}
%%
int yywrap(void){return 1;}

std::vector<Token> lexical_parser(const char *filename, light_buffer *buf) {
    yyin = fopen(filename, "r");
    _buf = read_file(filename);
    _tokens.clear();
    _buf.pointer = 0;
    yylex();
    buf = &_buf;
    return _tokens;
}