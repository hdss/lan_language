# Lan language
Это C-подобный язык, который умеет JIT-компилироватся в x86_64 код.

## Лексер
В качестве лексера был использован lex, конфиг для него лежит в папке lexical. [это он](lexial/lexical.l)
Лексер разбивает текст на лексемы, и позволяет разгрузить парсер грамматики. К примеру ему больше не нужно задумыватся о различных разделителях и пробельных симыолах.

## Парсер грамматики
Сама грамматика и ее парсер находятся в файле [parser.cpp](sources/parser.cpp). Основой послужил алгоритм рекурсивного спуска.

Пример (из файла test.gg)
![](tree.png)

Сама грамматика выглядит примерно так:
```
 * Grammar:
 * Program := Body$
 * Body := {{let}{Varlist};}[0-1]Function*
 * Function := Identifier{'('Varlist')'|'('')'}Compound
 * Compound := '{'Statement*'}'
 * Statement := Expression|IfStatement|WhileStatement|ReturnStatement|InOutStatement';'
 * Expression := RelationalExpression{[="+=""-="]RelationalExpression}*
 * RelationalExpression := AdditiveExpression{[<>"<="">=""=="]AdditiveExpression}*
 * AdditiveExpression := MultiplicativeExpression{[+-]MultiplicativeExpression}*
 * MultiplicativeExpression := SimpleExpression{[*div]SimpleExpression}*
 * SimpleExpression := '('Expression')'|Number|Identifier{'('Arglist')'|'()'}[0-1]
 * IfStatement := if'('Expression')'Compound
 * WhileStatement := while'('Expression')'Compound
 * ReturnStatement := "return"Expression
 * Varlist:={Identifier,}*Identifier}
 * Arglist:={Expression,}*Expression}
 ```

Прмер файла готовой программы:
```
let a, b, c, n, m;

fact() {
    if (n == 1) {
        return 1;
    }
    n -= 1;
    m = fact();
    n += 1;
    m *= n;
    return m;
}

main() {
    n = 7;
    a = 0;
    while (a < 1000000) {
        b = fact();
        a += 1;
    }
    return 0;
}$
```

После того как построится AST, мы делаем проверку семантических ошибок (к примеру неверное имя функции или переменной), это делается в файле [semantic](sources/semantic.cpp)
## Опкоды x86
В файле [opcodes](headers/OpCode.hpp) описаны опкоды x86_64. Их структура и они самы были взяты из [Intel Software Developer Guide](https://software.intel.com/sites/default/files/managed/39/c5/325462-sdm-vol-1-2abcd-3abcd.pdf)

## Elf файлы
Чтобы создать минимальный ELF файл, нужно написать в начале две структуры - ELF header и Program header. Они в моем случае описаны в файле [ElfCreator](sources/ElfCreator.cpp)

## Пример
В файле main.cpp написан минимальный возможный пример, который генерирует из файлов с расширением .gg (формат моего языка), готовые ELF файлы.
Также с помощью dot реализована возможность вывода AST

## Время работы
Сравнивалось время работы на софтверном процессоре [StackProcessor](https://gitlab.com/hdss/stackprocessor). Для этого использовался другой бэкенд [Compiler_virtual](sources/Compiler_virtual.cpp)

| Тип Бэкэнда   | Время работы (на файле test.gg) |
|:-------------:|:-------------------------------:| 
| virtual       |  0.774 |
| x86_64 |  0.070|