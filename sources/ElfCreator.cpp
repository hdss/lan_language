//
// Created by dm on 5/19/20.
//

#include "../headers/elf.hpp"

void Init(light_buffer& buf, FILE* file) {
  ELF_Header head;
  Program_Header phead;
  phead.P_FILESZ += buf.pointer;
  phead.P_MEMSZ += buf.pointer;
  fwrite(&head, sizeof(head), 1, file);
  fwrite(&phead, sizeof(phead), 1, file);
}