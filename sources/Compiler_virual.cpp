//
// Created by dm on 12/20/19.
//
#include "../headers/SymbolsTable.h"
#include "../headers/ASTree.h"

class Compiler {
public:
    typedef ASTree::Node Node;
    Compiler(SymbolsTable* table, ASTree*tree) : table(table), tree(tree) {}
    light_buffer compile();

private:
    SymbolsTable *table;
    ASTree *tree;
    int label_num;
    bool compile_pre(light_buffer *buf, Node* node);
    void compile_in(light_buffer *buf, Node* node);
    void compile_out(light_buffer *buf, Node*node);
    void dfs(Node *node, light_buffer*buf);
};

/*
 * enum class NodeType {
    ROOT,
    DATA,
    FUNC,
    ARG,
    STAT,
    COMPOUND,
    ADD,
    SUB,
    MUL,
    DIV,
    NUM,
    ASS,
    ADD_ASS,
    SUB_ASS,
    MUL_ASS,
    DIV_ASS,
    IF,
    WHILE,
    RETURN,
    LESS,
    GREATER,
    LEQ,
    GREQ,
    EQ,
    VAR,
    IN,
    OUT
 */
light_buffer Compiler::compile() {
    light_buffer buf(128);
    buf.append("call main\nend\n");
    dfs(tree->root, &buf);
    return buf;
}

void Compiler::dfs(Compiler::Node *node, light_buffer*buf) {
    if (!compile_pre(buf, node)) return;
    for (auto it : node->children) {
        if (it) {
            dfs(it, buf);
            if (it != node->children.back())
                compile_in(buf, node);
        }
    }
    compile_out(buf, node);
}

bool Compiler::compile_pre(light_buffer *buf, Compiler::Node *node) {
    char tmp[INT8_MAX];
    switch(node->type) {
        case NodeType :: FUNC:if (node->children[0]->type == NodeType::DATA) { buf->append(node->value); buf->append(":\n");} else
            {snprintf(tmp, INT8_MAX, "call %.*s\n", node->value.length, node->value.getPtr()); buf->append(tmp); return false;} break;
        //case NodeType :: COMPOUND: char tmp[INT8_MAX]; snprintf(tmp, INT8_MAX, ".comp%d:\n", label_num++); buf->append(tmp); break;
        case NodeType :: WHILE:  snprintf(tmp, INT8_MAX, ".while%d:\n", node->id); buf->append(tmp); break;
        case NodeType :: NUM :   snprintf(tmp, INT8_MAX, "push %.*s\n", node->value.length, node->value.getPtr()); buf->append(tmp); break;
        case NodeType :: VAR : snprintf(tmp, INT8_MAX, "pushr r%d\n", table->vars.find(node->value).second); buf->append(tmp); break;
        case NodeType :: IN : for (auto var : node->children[0]->children)
        {snprintf(tmp, INT8_MAX, "in\npop r%d\n", table->vars.find(var->value).second); buf->append(tmp);} return false;
        case NodeType :: DATA : return false;
    }
    return true;
}

void Compiler::compile_in(light_buffer *buf, Compiler::Node *node) {
    char tmp[INT8_MAX];
    switch(node->type) {
        case NodeType :: WHILE: snprintf(tmp, INT8_MAX, "push 0\nje .whilend%d\n", node->id); buf->append(tmp); break;
        case NodeType :: IF : snprintf(tmp, INT8_MAX, "push 0\nje .ifend%d\n", node->id); buf->append(tmp);
            break;
        case NodeType :: OUT : buf->append("out\n"); break;
    }
}

void Compiler::compile_out(light_buffer *buf, Compiler::Node *node) {
    char tmp[INT8_MAX];
    switch(node->type) {
        case NodeType :: WHILE: snprintf(tmp, INT8_MAX, "jmp .while%d\n.whilend%d:\n", node->id, node->id); buf->append(tmp); break;
        case NodeType :: IF : snprintf(tmp, INT8_MAX, ".ifend%d:\n", node->id); buf->append(tmp); break;
        case NodeType :: ADD : buf->append("add\n"); break;
        case NodeType :: SUB : buf->append("sub\n"); break;
        case NodeType :: MUL : buf->append("mul\n"); break;
        case NodeType :: DIV : buf->append("fdiv\n"); break;
        case NodeType :: LESS : buf->append("less\n"); break;
        case NodeType :: LEQ : buf->append("leq\n"); break;
        case NodeType :: GREQ : buf->append("greq\n"); break;
        case NodeType :: GREATER : buf->append("grt\n"); break;
        case NodeType :: EQ : buf->append("eq\n"); break;
        case NodeType :: ASS : snprintf(tmp, INT8_MAX, "pop r%d\npop r99\n", table->vars.find(node->children[0]->value).second); buf->append(tmp); break;
        case NodeType :: ADD_ASS : snprintf(tmp, INT8_MAX, "add\npop r%d\n", table->vars.find(node->children[0]->value).second); buf->append(tmp); break;
        case NodeType :: SUB_ASS : snprintf(tmp, INT8_MAX, "sub\npop r%d\n", table->vars.find(node->children[0]->value).second); buf->append(tmp);break;
        case NodeType :: MUL_ASS : snprintf(tmp, INT8_MAX, "mul\npop r%d\n", table->vars.find(node->children[0]->value).second); buf->append(tmp);break;
        case NodeType :: DIV_ASS : snprintf(tmp, INT8_MAX, "fdiv\npop r%d\n", table->vars.find(node->children[0]->value).second); buf->append(tmp);break;
        case NodeType :: RETURN : buf->append("ret\n"); break;
        case NodeType :: OUT : buf->append("out\n"); break;
    }
}