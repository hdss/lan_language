#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cassert>
#include "../headers/haifile.h"

char * light_string::getPtr() const {return buf->buffer + pos;}

char * light_buffer::curBuffer() {
    return buffer + pointer;
}


light_string light_buffer::copyToBuffer(light_string str) {
    light_string ans = {0};
    ans.pos = pointer;
    ans.buf = this;
    ans.length = str.length;
    append(str);
    return ans;
}
light_string light_buffer::copyToBuffer(const char *str) {
    light_string ans = {0};
    ans.pos = pointer;
    ans.buf = this;
    ans.length = strlen(str);
    append(str);
    return ans;
}
light_buffer::light_buffer(int initial_size) : buffer_size(initial_size), pointer(0) {
    buffer = (char *)calloc(initial_size * 2, sizeof(char));
}

light_buffer::light_buffer(char *buffer, int size, int pointer) :
        buffer(buffer),
        buffer_size(size),
        pointer(pointer) {}

light_buffer::light_buffer() : buffer(nullptr), buffer_size(0), pointer(0) {}

light_buffer::~light_buffer() {
    //  free(buffer);
}

char light_buffer::read(int pos) {
    assert(pos >= 0 && pos < buffer_size);
    return buffer[pos];
}

char light_buffer::getNext() {
    return read(pointer++);
}

void light_buffer::write(const char *str, int len, int pos) {
    if (pos + len + 1 >= buffer_size) {
        buffer_size *= 2;
        buffer = (char *)realloc(buffer, buffer_size);
    }
    memcpy(buffer + pos, str, len);
}

void light_buffer::append(const char *str, int len) {
    write(str, len, pointer);
    pointer += len;
}

void light_buffer::append(light_string str) {
    append(str.getPtr(), str.length);
}

void light_buffer::append(const char *str) {
    append(str, strlen(str));
}

bool light_buffer::is_end() const {
    return pointer == buffer_size;
}

void light_buffer::append(char chr) {
  append(&chr, 1);
}

const char &light_string::operator[](int index) const {
    assert(this);
    assert(index < length);
    assert(index >= 0);
    return *(this->getPtr() + index);
}

bool light_string::operator==(const light_string &str) const {
    if (length != str.length)
        return false;
    for (int i = 0; i < length; i++)
        if ((*this)[i] != str[i])
            return false;
    return true;
}

bool light_string::operator==(const char *str) const {
    if (strlen(str) == length)
        return strncmp(getPtr(), str, length) == 0;
    else
        return false;
}

LightStringDoubleHashFunc::LightStringDoubleHashFunc(size_t M) : M(M) {
    a1 = 257;  // Можно брать любые нечетные
    a2 = 263;
}

size_t LightStringDoubleHashFunc::operator()(const light_string &str, size_t probe) const {
    return (first_hash(str) + ((probe * second_hash(str)) % M)) % M;
}

size_t LightStringDoubleHashFunc::first_hash(const light_string &str) const {
    return string_hash(str, a1);
}

size_t LightStringDoubleHashFunc::second_hash(const light_string &str) const {
    return ((string_hash(str, a2) << 1) | 1) % M;
}

size_t LightStringDoubleHashFunc::string_hash(const light_string &str, size_t a) const {
    size_t ans = 0;
    for (int i = 0; i < str.length; i++) {
        ans = (ans * a + str[i]) % M;
    }
    return ans;
}

light_buffer read_file(const char* filename) {
    assert(filename);
    FILE *input_file = fopen(filename, "r");
    assert(input_file);

    fseek(input_file, 0, SEEK_END);
    int file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);

    char *buffer = (char *)calloc(file_size + 1, sizeof(char));
    assert(buffer);

    file_size = fread(buffer, 1, file_size, input_file);

    buffer[file_size] = 0;
    fclose(input_file);
    return {buffer, file_size + 1, 0};
}

light_string *divide_text(light_buffer *buffer, int size, int *lines_number) {
    assert(buffer);

    int string_count = 1;
    for (int i = 0; i < size; i++) {
        if (buffer->buffer[i] == '\n') {
            string_count++;
        }
    }

    light_string *text =
            (light_string *)calloc(string_count + 1, sizeof(light_string));
    text[string_count].pos = -1;
    text[string_count].buf = nullptr;
    text[0].pos = 0;
    text[0].buf = buffer;
    int current_line = 1;
    for (int i = 0; i < size; i++) {
        if (buffer->buffer[i] == '\n') {
            text[current_line].pos = (i + 1);
            text[current_line].buf = buffer;
            text[current_line - 1].length =
                    text[current_line].pos - text[current_line - 1].pos - 1;

            current_line++;
            buffer->buffer[i] = '\0';
        }
    }

    *lines_number = string_count - 1;
    return text;
}