//
// Created by dm on 12/20/19.
//
#include "../headers/SymbolsTable.h"
#include "../headers/ASTree.h"
#include "../headers/OpCode.hpp"
#include "../headers/IOlib.hpp"
#include "../headers/util.hpp"


class Compiler {
public:
    typedef ASTree::Node Node;
    Compiler(SymbolsTable* table, ASTree*tree) : table(table), tree(tree) {}
    light_buffer compile();

private:
    SymbolsTable *table;
    ASTree *tree;
    bool compile_pre(std::vector<Instruction*>& code, Node* node, light_buffer& buf);
    void compile_in(std::vector<Instruction*>& code, Node* node, light_buffer& buf);
    void compile_out(std::vector<Instruction*>& code, Node*node, light_buffer& buf);
    void dfs(Node *node, std::vector<Instruction*>& code, light_buffer& buf);
    void prepare(light_buffer& buf, vector<Instruction*>& instructions );
};

/*
 * enum class NodeType {
    ROOT,
    DATA,
    FUNC,
    ARG,
    STAT,
    COMPOUND,
    ADD,
    SUB,
    MUL,
    DIV,
    NUM,
    ASS,
    ADD_ASS,
    SUB_ASS,
    MUL_ASS,
    DIV_ASS,
    IF,
    WHILE,
    RETURN,
    LESS,
    GREATER,
    LEQ,
    GREQ,
    EQ,
    VAR,
    IN,
    OUT
 */

void Compiler::prepare(light_buffer& buf, vector<Instruction*>& instructions ) {
  instructions.push_back(new MOV(Args(R12, RSP)));
  instructions.back()->write(buf);
  instructions.push_back(new MOV(Args(RAX, table->vars_sz * 8)));
  instructions.back()->write(buf);
  instructions.push_back(new SUB(Args(RSP, RAX)));
  instructions.back()->write(buf);
  instructions.push_back(new CALL(table->funcs.find(light_buffer(128).copyToBuffer("main")).second->addr - buf.pointer - 5));
  instructions.back()->write(buf);
  produce(buf, Exit, instructions);
}

light_buffer Compiler::compile() {
    light_buffer buf(128);
    size_t cur_addr = 0;
    std::vector<Instruction*> instructions;
    prepare(buf, instructions);
    dfs(tree->root, instructions, buf);
    instructions.clear();
    buf = light_buffer(128);
    prepare(buf, instructions);
    dfs(tree->root, instructions, buf);
    for (auto&& it : instructions)
      std::cerr << it->AsText() << std::endl;
    return buf;
}

void Compiler::dfs(Node* node, std::vector<Instruction*>& code, light_buffer& buf) {
    if (!compile_pre(code, node, buf)) return;
    for (auto it : node->children) {
        if (it) {
            dfs(it, code, buf);
            if (it != node->children.back())
                compile_in(code, node, buf);
        }
    }
    compile_out(code, node, buf);
}

bool Compiler::compile_pre(std::vector<Instruction*>& code, Compiler::Node *node, light_buffer& buf) {
    char tmp[INT8_MAX];
    switch(node->type) {
        case NodeType :: FUNC:
          if (node->children[0]->type == NodeType::DATA) {
            node->addr = buf.pointer;
          } else {
            auto fun = table->funcs.find(node->value);
            code.push_back(new CALL(fun.second->addr - buf.pointer - 5));
            code.back()->write(buf);
            code.push_back(new PUSH(RAX));
            code.back()->write(buf);
            return false;
          };
        case NodeType :: WHILE: node->addr = buf.pointer; break;
        case NodeType :: NUM : {
          long long num = strtol(node->value.getPtr(), nullptr, 10);
          code.push_back(new MOV(Args(RAX, num)));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: VAR : {
          code.push_back(new MOV(Args(RBX, R12)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RAX, RBX, -8 * (table->vars.find(node->value).second + 1))));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: IN :
          for (auto var : node->children[0]->children) {
            code.push_back(new MOV(Args(RSI, R12)));
            code.back()->write(buf);
            code.push_back(new MOV(Args(RAX, table->vars.find(var->value).second * 8 + 8)));
            code.back()->write(buf);
            code.push_back(new SUB(Args(RSI, RAX)));
            code.back()->write(buf);
            produce(buf, InChar, code);
            return false;
          }
        case NodeType :: DATA : return false;
    }
    return true;
}

void Compiler::compile_in(std::vector<Instruction*>& code, Compiler::Node *node, light_buffer& buf) {
    char tmp[INT8_MAX];
    switch(node->type) {
        case NodeType :: WHILE: {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RBX, 0)));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RAX, RBX)));
          code.back()->write(buf);
          code.push_back(new JE(node->end_addr - buf.pointer - 6));
          code.back()->write(buf);
          break;
        }
        case NodeType :: IF : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RBX, 1)));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RAX, RBX)));
          code.back()->write(buf);
          code.push_back(new JNE(node->end_addr - buf.pointer - 6));
          code.back()->write(buf);
          break;
        }
        case NodeType :: OUT : {
          code.push_back(new MOV(Args(RSI, RSP)));
          code.back()->write(buf);
          produce(buf, OutChar, code);
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          break;
        }
    }
}

void Compiler::compile_out(std::vector<Instruction*>& code, Compiler::Node *node, light_buffer& buf) {
    char tmp[INT8_MAX];
    switch(node->type) {
        case NodeType :: WHILE: {
          code.push_back(new JMP(node->addr - buf.pointer - 5));
          code.back()->write(buf);
          node->end_addr = buf.pointer;
          break;
        }
        case NodeType :: IF : node->end_addr = buf.pointer; break;
        case NodeType :: ADD : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new ADD(Args(RAX, RBX)));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: SUB : {
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new SUB(Args(RAX, RBX)));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: MUL : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new IMUL(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: DIV : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RDX, 0)));
          code.back()->write(buf);
          code.push_back(new IDIV(RBX));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: LESS : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new SETL(RAX));
          code.back()->write(buf);
          code.push_back(new MOVZX(RAX, RAX));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: LEQ : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new SETLE(RAX));
          code.back()->write(buf);
          code.push_back(new MOVZX(RAX, RAX));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: GREQ : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new SETGE(RAX));
          code.back()->write(buf);
          code.push_back(new MOVZX(RAX, RAX));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: GREATER : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new SETG(RAX));
          code.back()->write(buf);
          code.push_back(new MOVZX(RAX, RAX));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: EQ : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new CMP(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new SETE(RAX));
          code.back()->write(buf);
          code.push_back(new MOVZX(RAX, RAX));
          code.back()->write(buf);
          code.push_back(new PUSH(RAX));
          code.back()->write(buf);
          break;
        }
        case NodeType :: ASS : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, R12)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, -table->vars.find(node->children[0]->value).second * 8 - 8, RAX)));
          code.back()->write(buf);
          break;
        }
        case NodeType :: ADD_ASS : {
            code.push_back(new POP(RAX));
            code.back()->write(buf);
            code.push_back(new POP(RBX));
            code.back()->write(buf);
            code.push_back(new ADD(Args(RAX, RBX)));
            code.back()->write(buf);
            code.push_back(new MOV(Args(RCX, R12)));
            code.back()->write(buf);
            code.push_back(new MOV(Args(RCX, -table->vars.find(node->children[0]->value).second * 8 - 8, RAX)));
            code.back()->write(buf);
            break;
        }
        case NodeType :: SUB_ASS : {
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new SUB(Args(RAX, RBX)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, R12)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, -table->vars.find(node->children[0]->value).second * 8 - 8, RAX)));
          code.back()->write(buf);
          break;
        }
        case NodeType :: MUL_ASS : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new IMUL(Args(RBX, RAX)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, R12)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, -table->vars.find(node->children[0]->value).second * 8 - 8, RAX)));
          code.back()->write(buf);
          break;
        }
        case NodeType :: DIV_ASS : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new POP(RBX));
          code.back()->write(buf);
          code.push_back(new IDIV(RBX));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, R12)));
          code.back()->write(buf);
          code.push_back(new MOV(Args(RCX, -table->vars.find(node->children[0]->value).second * 8 - 8, RAX)));
          code.back()->write(buf);
          break;
        }
        case NodeType :: RETURN : {
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          code.push_back(new RET());
          code.back()->write(buf);
          break;
        }
        case NodeType :: OUT : {
          code.push_back(new MOV(Args(RSI, RSP)));
          code.back()->write(buf);
          produce(buf, OutChar, code);
          code.push_back(new POP(RAX));
          code.back()->write(buf);
          break;
        }
    }
}
