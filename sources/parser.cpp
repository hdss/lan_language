#include "../headers/ASTree.h"
#include "../headers/tokens.hpp"
#include <vector>
using std::vector;
/*
 * Grammar:
 * Program := Body$
 * Body := {{let}{Varlist};}[0-1]Function*
 * Function := Identifier{'('Varlist')'|'('')'}Compound
 * Compound := '{'Statement*'}'
 * Statement := Expression|IfStatement|WhileStatement|ReturnStatement|InOutStatement';'
 * Expression := RelationalExpression{[="+=""-="]RelationalExpression}*
 * RelationalExpression := AdditiveExpression{[<>"<="">=""=="]AdditiveExpression}*
 * AdditiveExpression := MultiplicativeExpression{[+-]MultiplicativeExpression}*
 * MultiplicativeExpression := SimpleExpression{[*div]SimpleExpression}*
 * SimpleExpression := '('Expression')'|Number|Identifier{'('Arglist')'|'()'}[0-1]
 * IfStatement := if'('Expression')'Compound
 * WhileStatement := while'('Expression')'Compound
 * ReturnStatement := "return"Expression
 * Varlist:={Identifier,}*Identifier}
 * Arglist:={Expression,}*Expression}
 */

class ASTBuilder {
    int position;
    int cur_size;
    vector<Token> tokens;
    typedef ASTree::Node Node;
    Node* getProgram();
    Node* getBody();
    Node* getFunction();
    Node* getCompound();
    Node *getStatement();
    Node* getExpression();
    Node* getRExpression();
    Node* getAExpression();
    Node* getMExpression();
    Node* getSExpression();
    Node *getIf();
    Node *getWhile();
    Node *getReturn();
    Node *getIO();
    Node* getVarlist();
    Node* getArglist();
    void raiseSyntaxError(const char* def);
public:
    ASTree buildASTree();
    ASTBuilder(const vector<Token> tokens) : position(0), cur_size(0), tokens(tokens) {};
    NodeType isRel(light_string *pString);
    NodeType isAss(light_string *pString);
    NodeType isAdd(light_string *pString);
    NodeType isMul(light_string *pString);

    NodeType isIO(light_string *pString);
};

ASTBuilder::Node *ASTBuilder::getProgram() {
    Node* node = getBody();
    if (!node)
        return nullptr;
    if(tokens[position].value == "$")
        return node;
    else
          return nullptr;
}

ASTBuilder::Node *ASTBuilder::getBody() {
    Node* result = new Node(NodeType::ROOT, cur_size);
    cur_size++;
    if (tokens[position].value == "let") {
        position++;
        Node* vars = getVarlist();
        result->children.push_back(vars);
        if (tokens[position].value == ";")
            position++;
        else
            raiseSyntaxError("; missing");
    }
    Node *chld = nullptr;
    while((chld = getFunction())) {
        result->children.push_back(chld);
    }
    return result;
}

ASTBuilder::Node *ASTBuilder::getFunction() {
    Node *result = nullptr;
    if (tokens[position].type == TokenType::ID) {
        result = new Node(NodeType::FUNC, cur_size);
        cur_size++;
        result->value = tokens[position].value;
        position++;
        if (tokens[position].value == "(") {
            position++;
            if (tokens[position].value == ")") {
                Node *args = new Node(NodeType::DATA, cur_size);
                cur_size++;
                result->children.push_back(args);
            } else {
                Node *args = getVarlist();
                result->children.push_back(args);
            }
            if (tokens[position].value == ")") {
                position++;
                Node *f_body = getCompound();
                if (f_body)
                    result->children.push_back(f_body);
                return result;
            }
        } else {
            raiseSyntaxError("\"(\" symbol missed in function defenition");
        }
    }
    return result;
}

ASTBuilder::Node *ASTBuilder::getCompound() {
    Node *node = nullptr;
    if (tokens[position].value == "{") {
        node = new Node(NodeType::COMPOUND, cur_size);
        cur_size++;
        position++;
        Node *state = nullptr;
        while((state = getStatement())) {
            node->children.push_back(state);
        }
        if (tokens[position].value == "}") {
            position++;
            return node;
        } else
            raiseSyntaxError("} missing on compound");

    } else {
        raiseSyntaxError("{ missing on compound expression");
    }
    if (node)
        cur_size--;
    delete node;
    return nullptr;
}

ASTBuilder::Node *ASTBuilder::getExpression() {
    Node* node = getRExpression();
    NodeType symtype = NodeType::ROOT;
    if (node == nullptr)
        return node;
    while(position < tokens.size() && (bool)(symtype = isAss(&tokens[position].value)) ) {
        position++;
        Node* seq = getRExpression();
        Node *new_node = new Node(symtype, cur_size);
        cur_size++;
        new_node->children.push_back(node);
        new_node->children.push_back(seq);
        node = new_node;
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getRExpression() {
    Node* node = getAExpression();
    if (node == nullptr)
        return node;
    NodeType symtype = NodeType::ROOT;
    while(position < tokens.size() && (bool)(symtype = isRel(&tokens[position].value)) ) {
        position++;
        Node* seq = getAExpression();
        Node *new_node = new Node(symtype, cur_size);
        cur_size++;
        new_node->children.push_back(node);
        new_node->children.push_back(seq);
        node = new_node;
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getAExpression() {
    Node* node = getMExpression();
    if (node == nullptr)
        return node;
    NodeType symtype = NodeType::ROOT;
    while(position < tokens.size() && (bool)(symtype = isAdd(&tokens[position].value)) ) {
        position++;
        Node* seq = getMExpression();
        Node *new_node = new Node(symtype, cur_size);
        cur_size++;
        new_node->children.push_back(node);
        new_node->children.push_back(seq);
        node = new_node;
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getMExpression() {
    Node* node = getSExpression();
    if (node == nullptr)
        return node;
    NodeType symtype = NodeType::ROOT;
    while(position < tokens.size() && (bool)(symtype = isMul(&tokens[position].value)) ) {
        position++;
        Node* seq = getSExpression();
        Node *new_node = new Node(symtype, cur_size);
        cur_size++;
        new_node->children.push_back(node);
        new_node->children.push_back(seq);
        node = new_node;
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getSExpression() {
    if (tokens[position].value == "(") {
        position++;
        Node *expr = getExpression();
        if (tokens[position].value == ")")
            position++;
        else
            raiseSyntaxError(") missing");
        return expr;
    }
    if (tokens[position].type == TokenType::NUMBER) {
        Node *num = new Node(NodeType::NUM, cur_size);
        cur_size++;
        num->value = tokens[position].value;
        position++;
        return num;
    }
    if (tokens[position].type == TokenType::ID) {
        Node *var = new Node(NodeType::VAR, cur_size);
        cur_size++;
        var->value = tokens[position].value;
        position++;
        if (tokens[position].value == "(") {
            position++;
            var->type = NodeType :: FUNC;
            if (tokens[position].value == ")") {
                Node *args = new Node(NodeType::ARG, cur_size);
                cur_size++;
                var->children.push_back(args);
            } else {
                Node *args = getArglist();
                var->children.push_back(args);
            }
            if (tokens[position].value == ")")
                position++;
            else
                raiseSyntaxError(") missing");
        }
        return var;
    }
    return nullptr;
}

ASTBuilder::Node *ASTBuilder::getIf() {
    Node *node = nullptr;
    if(tokens[position].value == "if") {
        position++;
        if (tokens[position].value == "(") {
            position++;
            Node *cond = getExpression();
            if (tokens[position].value == ")") {
                position++;
                Node *body = getCompound();
                node = new Node(NodeType::IF, cur_size);
                cur_size++;
                node->children.push_back(cond);
                node->children.push_back(body);
                return node;
            } else
                raiseSyntaxError(") expected");
        } else
            raiseSyntaxError("( expected");
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getWhile() {
    Node *node = nullptr;
    if(tokens[position].value == "while") {
        position++;
        if (tokens[position].value == "(") {
            position++;
            Node *cond = getExpression();
            if (tokens[position].value == ")") {
                position++;
                Node *body = getCompound();
                node = new Node(NodeType::WHILE, cur_size);
                cur_size++;
                node->children.push_back(cond);
                node->children.push_back(body);
                return node;
            } else
                raiseSyntaxError(") expected");
        } else
            raiseSyntaxError("( expected");
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getReturn() {
    Node *node = nullptr;
    if(tokens[position].value == "return") {
        position++;
        Node *cond = getExpression();
        if (!cond) {
            raiseSyntaxError("expression expected");
        }
        node = new Node(NodeType::RETURN, cur_size);
        cur_size++;
        node->children.push_back(cond);
        return node;
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getVarlist() {
    Node *node = new Node(NodeType::DATA, cur_size);
    cur_size++;
    if (tokens[position].type == TokenType::ID) {
        do {
            Node *var = new Node(NodeType::VAR, cur_size);
            cur_size++;
            var->value = tokens[position].value;
            node->children.push_back(var);
            position++;
        }while(tokens[position++].value == ",");
        position--;
    } else {
        raiseSyntaxError("variables expected");
    }
    return node;
}

ASTBuilder::Node *ASTBuilder::getArglist() {
    Node *node = new Node(NodeType::ARG, cur_size);
    cur_size++;
    Node *arg = getExpression();
    if (arg) {
        node->children.push_back(arg);
        while(tokens[position].value == ",") {
            position++;
            arg = getExpression();
            node->children.push_back(arg);
        }
    } else {
        raiseSyntaxError("argument missing");
    }
    return node;
}

ASTree ASTBuilder::buildASTree() {
    ASTree tree;
    tree.root = getProgram();
    tree.size = cur_size;
    return tree;
}

ASTBuilder::Node *ASTBuilder::getStatement() {
    Node *node = getExpression();
    if (!node) {
        node = getIf();
        if (!node) {
            node = getWhile();
            if (!node) {
                node = getReturn();
                if (!node) {
                    node = getIO();
                }
            }
        }
    }
    if (!node)
        return node;
    if (node->type != NodeType::IF && node->type != NodeType::WHILE)
        if (tokens[position].value == ";") {
            position++;
        } else
            raiseSyntaxError("; missing");
    return node;
}

NodeType ASTBuilder::isRel(light_string *pString) {
    if (*pString == "<")
        return NodeType :: LESS;
    if (*pString == ">")
        return NodeType :: GREATER;
    if (*pString == "<=")
        return NodeType :: LEQ;
    if (*pString == ">=")
        return NodeType :: GREQ;
    if (*pString == "==")
        return NodeType :: EQ;
    return NodeType :: ROOT;
}

NodeType ASTBuilder::isAss(light_string *pString) {
    if (*pString == "=")
        return NodeType :: ASS;
    if (*pString == "+=")
        return NodeType :: ADD_ASS;
    if (*pString == "-=")
        return NodeType :: SUB_ASS;
    if (*pString == "*=")
        return NodeType :: MUL_ASS;
    if (*pString == "/=")
        return NodeType :: DIV_ASS;
    return NodeType :: ROOT;
}

NodeType ASTBuilder::isMul(light_string *pString) {
    if (*pString == "*")
        return NodeType :: MUL;
    if (*pString == "/")
        return NodeType :: DIV;
    return NodeType :: ROOT;
}

NodeType ASTBuilder::isAdd(light_string *pString) {
    if (*pString == "+")
        return NodeType :: ADD;
    if (*pString == "-")
        return NodeType :: SUB;
    return NodeType :: ROOT;
}

void ASTBuilder::raiseSyntaxError(const char *def) {
    fprintf(stderr, "ERROR: %s\n", def);
}

ASTBuilder::Node *ASTBuilder::getIO() {
    Node *result = nullptr;
    if ((bool)isIO(&tokens[position].value)) {
        result = new Node(isIO(&tokens[position].value), cur_size);
        cur_size++;
        position++;
        Node* vars = nullptr;
        if (result->type == NodeType :: IN)
            vars = getVarlist();
        else
            vars = getArglist();
        result->children.push_back(vars);
    }
    return result;
}

NodeType ASTBuilder::isIO(light_string *pString) {
    if (*pString == "in")
        return NodeType :: IN;
    if (*pString == "out")
        return NodeType :: OUT;
    return NodeType :: ROOT;
}