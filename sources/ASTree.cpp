#pragma once
#define _GNU_SOURCE
#include <stdio.h>
#include <ctype.h>
#include "../headers/ASTree.h"

const char CONF_PATH[] = "gr.conf.dot";
const int MAX_LEN = 200;
int _sz = 0;

ASTree::Node* ASTree::Node::make_copy(const Node *node) const{
    Node *result = new Node(node->type, node->id);
    for (auto it : node->children)
        if (it)
            result->children.push_back(make_copy(it));
    return result;
}

char *ASTree::Node::getText() {
    char *buf = (char *) calloc(MAX_LEN, sizeof(char));
    switch (type) {
        case NodeType::COMPOUND : return snprintf(buf, MAX_LEN, "COMPOUND"), buf;
        case NodeType::ROOT: return snprintf(buf, MAX_LEN, "ROOT"), buf;
        case NodeType::DATA: return snprintf(buf, MAX_LEN, "DATA"), buf;
        case NodeType::ARG: return snprintf(buf, MAX_LEN, "ARG"), buf;
        case NodeType::ADD: return snprintf(buf, MAX_LEN, "+"), buf;
        case NodeType::SUB: return snprintf(buf, MAX_LEN, "-"), buf;
        case NodeType::MUL: return snprintf(buf, MAX_LEN, "*"), buf;
        case NodeType::DIV: return snprintf(buf, MAX_LEN, "/"), buf;
        case NodeType::ASS: return snprintf(buf, MAX_LEN, "="), buf;
        case NodeType::ADD_ASS: return snprintf(buf, MAX_LEN, "+="), buf;
        case NodeType::SUB_ASS: return snprintf(buf, MAX_LEN, "-="), buf;
        case NodeType::MUL_ASS: return snprintf(buf, MAX_LEN, "*="), buf;
        case NodeType::DIV_ASS: return snprintf(buf, MAX_LEN, "/="), buf;
        case NodeType::IF: return snprintf(buf, MAX_LEN, "if"), buf;
        case NodeType::WHILE: return snprintf(buf, MAX_LEN, "while"), buf;
        case NodeType::RETURN: return snprintf(buf, MAX_LEN, "return"), buf;
        case NodeType::LESS: return snprintf(buf, MAX_LEN, "\\>"), buf;
        case NodeType::GREATER: return snprintf(buf, MAX_LEN, "\\>"), buf;
        case NodeType::LEQ: return snprintf(buf, MAX_LEN, "\\<="), buf;
        case NodeType::GREQ: return snprintf(buf, MAX_LEN, "\\>="), buf;
        case NodeType::EQ: return snprintf(buf, MAX_LEN, "=="), buf;
        case NodeType::IN: return snprintf(buf, MAX_LEN, "in"), buf;
        case NodeType::OUT: return snprintf(buf, MAX_LEN, "out"), buf;
        default: return snprintf(buf, value.length+1, "%s", value.getPtr()), buf;
    }
}

ASTree::ASTree() : size(0) {
    root = nullptr;
}

ASTree::Node *ASTree::make_copy(const ASTree::Node &node) const{
    return node.make_copy(&node);
}

void ASTree::printDfs(ASTree::Node *node, FILE *file) {
    char *name = node->getText();
    fprintf(file, "node%d [label=\"{%#010x | %s }\"]\n", node->id, node, name);
    free(name);
    for (auto child : node->children)
        if (child) {
            fprintf(file, "node%d -> node%d\n", node->id, child->id);
            printDfs(child, file);
        }
}

void ASTree::print(const char *filename) {
    FILE *file = fopen(".tmpPrintTree.dot", "w");
    light_buffer temp_buf = read_file(CONF_PATH);
    fwrite(temp_buf.buffer, sizeof(char), temp_buf.buffer_size - 1, file);
    printDfs(root, file);
    fprintf(file, "}\n");
    fclose(file);
    char tmp[1024] = {0};
    sprintf(tmp, "dot -Tpng .tmpPrintTree.dot -o %s", filename);
    system(tmp);
}

void ASTree::conv() {
    while (convolute(&root)) {}

}

void print(NodeType type, light_string *data, light_buffer *buffer, bool if_data=false) {
    if (type == NodeType :: COMPOUND)
        buffer->append("COMPOUND");
    if (type == NodeType :: DATA) {
        if (if_data)
            buffer->append("data");
        else
            buffer->append("ARG");
    }
    if (type == NodeType :: ARG)
        buffer->append("ARG");
    if (type == NodeType :: ADD)
        buffer->append("+");
    if (type == NodeType :: SUB)
        buffer->append("-");
    if (type == NodeType :: MUL)
        buffer->append("*");
    if (type == NodeType :: DIV)
        buffer->append("/");
    if (type == NodeType :: IF)
        buffer->append("if");
    if (type == NodeType :: WHILE)
        buffer->append("while");
    if (type == NodeType :: RETURN)
        buffer->append("return");
    if (type == NodeType :: ADD_ASS)
        buffer->append("+=");
    if (type == NodeType :: SUB_ASS)
        buffer->append("-=");
    if (type == NodeType :: MUL_ASS)
        buffer->append("*=");
    if (type == NodeType :: ASS)
        buffer->append("=");
    if (type == NodeType :: EQ)
        buffer->append("==");
    if (type == NodeType :: LESS)
        buffer->append("<");
    if (type == NodeType :: GREATER)
        buffer->append(">");
    if (type == NodeType :: GREQ)
        buffer->append(">=");
    if (type == NodeType :: LEQ)
        buffer->append("<=");
    if (type == NodeType :: ROOT)
        buffer->append("ROOT");
    if (type == NodeType :: IN)
        buffer->append("in");
    if (type == NodeType :: OUT)
        buffer->append("out");
    if (type == NodeType :: NUM || type == NodeType::FUNC || type == NodeType::VAR)
        buffer->append(*data);
}

bool ASTree::convolute(ASTree::Node **node) {
    return false;
}

void ASTree::writeDfs(ASTree::Node *node, light_buffer *buffer, bool if_data = false) {
    buffer->append("{\n");
    buffer->append("\"", 1);
    ::print(node->type, &node->value, buffer, if_data);
    buffer->append("\"");
    char *buf = nullptr;
    asprintf(&buf, " %d\n", node->children.size());
    buffer->append(buf);
    for (auto child : node->children) {
        writeDfs(child, buffer, node->type == NodeType::ROOT);
    }
    buffer->append("}\n");
}

light_buffer ASTree::serialize() {
    light_buffer buf(128);
    writeDfs(root, &buf);
    return buf;
}


int ASTree::nextChar(light_buffer *buffer) {
    char c = 0;
    while (isspace(c = buffer->getNext()));
    if (!c)
        return -1;
    return c;
}

light_string ASTree::readData(light_buffer *buffer) {
    light_string data = {0};
    data.pos = 0;
    data.buf = buffer;
    int c = nextChar(buffer);
    if (c == -1)
        return data;
    if (c != '\"')
        return data;
    data.pos = buffer->pointer;
    c = buffer->getNext();
    int len = 0;
    while (c && c != '\"') {
        len++;
        c = buffer->getNext();
    }
    data.length = len;
    return data;
}

NodeType getType(light_string data, bool is_func) {
    NodeType type = NodeType :: ROOT;
    if (data == "COMPOUND")
        return NodeType :: COMPOUND;
    if (data == "DATA")
        return NodeType :: DATA;
    if (data == "ARG")
        return NodeType :: ARG;
    if (data == "+")
        return NodeType :: ADD;
    if (data == "-")
        return NodeType ::SUB;
    if (data == "*")
        return NodeType ::MUL;
    if (data == "/")
        return NodeType ::DIV;
    if (data == "if")
        return NodeType ::IF;
    if (data == "while")
        return NodeType ::WHILE;
    if (data == "return")
        return NodeType :: RETURN;
    if (data == "+=")
        return NodeType :: ADD_ASS;
    if (data == "-=")
        return NodeType :: SUB_ASS;
    if (data == "*=")
        return NodeType :: MUL_ASS;
    if (data == "/=")
        return NodeType ::DIV_ASS;
    if (data == "=")
        return NodeType ::ASS;
    if (data == "<")
        return NodeType ::LESS;
    if (data == ">")
        return NodeType ::GREATER;
    if (data == "<=")
        return NodeType :: LEQ;
    if (data == ">=")
        return NodeType :: GREQ;
    if (data == "ROOT")
        return NodeType :: ROOT;
    if (data == "IN")
        return NodeType :: IN;
    if (data == "OUT")
        return NodeType :: OUT;
    if (data[0] >= '0' && data[0] <= '9')
        return NodeType :: NUM;
    if (is_func)
        return NodeType :: FUNC;
    return NodeType :: VAR;
}

ASTree::Node* readDfs(light_buffer *buf, bool is_func) {
    char c = ASTree::nextChar(buf);
    typedef ASTree::Node Node;
    Node *node = new Node(NodeType::ROOT, _sz++);
    light_string data = ASTree::readData(buf);
    NodeType type = getType(data, is_func);
    int children = 0;
    sscanf(buf->curBuffer(), "%d", &children);
    for (int i = 0; i < children; i++) {
        node->children.push_back(readDfs(buf, type != NodeType::DATA));
    }
    return node;
}

ASTree deserialize(light_buffer *buf) {
    typedef ASTree::Node Node;
    _sz = 0;
    ASTree tree;
    tree.root = readDfs(buf, 1);
    tree.size = _sz;
    return tree;
}

void ASTree::saveTree(const char *filename) {
    FILE *file = fopen(filename, "w");
    light_buffer buf(128);
    writeDfs(root, &buf);
    fwrite(buf.buffer, 1, buf.pointer, file);
    fclose(file);
}

