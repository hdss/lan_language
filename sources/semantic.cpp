#include "../headers/haifile.h"
#include "../headers/hash_table.hpp"
#include "../headers/ASTree.h"
#include "../headers/SymbolsTable.h"
enum class SemanticError {
    OK,
    UNDEF_FUN,
    UNDEF_VAR,
    AMB_FUN,
    AMB_VAR,
    BAD_ASS
};

class SemanticChecker {
public:
    SemanticChecker(ASTree &ast);
    void doCheck();
    SymbolsTable* passTable();
private:
    SymbolsTable *table;
    typedef ASTree::Node Node;
    void dfs(Node *node);
    ASTree *cur_tree;
    SemanticError _errno;
    void raiseSemError(const light_string &name);
};

void SemanticChecker::doCheck() {
    dfs(cur_tree->root);
}

SemanticChecker::SemanticChecker(ASTree &ast) {
    cur_tree = &ast;
    table = new SymbolsTable();
    table->funcs_sz = 0;
    table->vars_sz = 0;
    _errno = SemanticError :: OK;
    for (auto fun : ast.root->children) {
        if (fun->type == NodeType::FUNC) {
            if (table->funcs.find(fun->value).first) {
                _errno = SemanticError :: AMB_FUN;
                raiseSemError(fun->value);
            } else {
                table->funcs.insert(fun->value, fun);
            }
        }
        if (fun->type == NodeType::DATA) {
            for (auto var : fun->children) {
                if (table->vars.find(var->value).first) {
                    _errno = SemanticError ::AMB_VAR;
                    raiseSemError(var->value);
                } else {
                    table->vars.insert(var->value, table->vars_sz++);
                }
            }
        }
    }
}

void SemanticChecker::dfs(SemanticChecker::Node *node) {
    if (node->type == NodeType::FUNC) {
        if(!table->funcs.find(node->value).first) {
            _errno = SemanticError ::UNDEF_FUN;
            raiseSemError(node->value);
        }
    }
    if (node->type == NodeType::VAR) {
        if(!table->vars.find(node->value).first) {
            _errno = SemanticError ::UNDEF_VAR;
            raiseSemError(node->value);
        }
    }
    if (node->type == NodeType::ASS || node->type == NodeType::ADD_ASS || node->type == NodeType::SUB_ASS || node->type == NodeType::MUL_ASS ||
            node->type == NodeType::DIV_ASS) {
        //флаги груп
        if (node->children[0]->type != NodeType::VAR) {
            _errno = SemanticError :: BAD_ASS;
            raiseSemError(node->value);
        }
    }
    for (auto child : node->children)
        if (child)
            dfs(child);
}

void SemanticChecker::raiseSemError(const light_string &name) {
    switch (_errno) {
        case SemanticError :: AMB_VAR: fprintf(stderr, "This var name already used: %*s", name.length, name.getPtr()); break;
        case SemanticError :: AMB_FUN: fprintf(stderr, "This function name already used: %*s", name.length, name.getPtr()); break;
        case SemanticError :: UNDEF_VAR: fprintf(stderr, "This var name never defined: %*s", name.length, name.getPtr()); break;
        case SemanticError :: UNDEF_FUN: fprintf(stderr, "This function never seen: %*s", name.length, name.getPtr()); break;
        case SemanticError :: BAD_ASS: fprintf(stderr, "Assignment not to lvalue: %*s", name.length, name.getPtr()); break;
    }
}

SymbolsTable *SemanticChecker::passTable() {
    return table;
}
